# README #

nlq: the natural language question

nla: the ground-truth natural language answer

template_type: the type of the template that is used to parse the question

img_id: the image id for the COCO Validation set.


Please Cite: Peng Wang, Qi Wu, Chunhua Shen, Anton van den Hengel, Anthony Dick. Explicit knowledge-based reasoning for visual question answering, International Joint Conference on Artificial Intelligence (IJCAI'17), 2017
